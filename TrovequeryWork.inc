<?php

use Drupal\trove\troveApi;
use Drupal\trove\TroveApiWorkRecord;

class TrovequeryWork extends TroveApiWorkRecord {

  public function parse($joins = NULL) {
    $results = array();
    $results[] = $this->buildRow($this->response['work']);
    return $results;
  }

  function buildRow($work) {
    $row = new stdClass;
    foreach ($work as $key => $value) {
      $row->{$key} = $value;
    }
    return $row;
  }

}
